let nums1 = [46,55,88,0,0,0,0]
let nums2 = [18,29,80,90]

let m =3;
let n=4;

merge(nums1,m,nums2,n)

/**  
 * @param {number[]} nums1 - первый отсортированный массив  
 * @param {number} m - количество значимых элементов в nums1  
 * @param {number[]} nums2 - второй отсортированный массив  
 * @param {number} n - количество элементов в nums2  
 * @return {void} Не возвращайте ничего, вместо этого модифицируйте nums1.  
 */  
function merge(nums1, m, nums2, n) {  
  nums1.splice(m)

   nums2.forEach((each)=>{
     nums1.push(each)
   })

   nums1.sort((a,b)=>a-b)
   console.log(nums1)
}